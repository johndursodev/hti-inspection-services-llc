import React from 'react'
import { Link } from 'gatsby'

import Layout from '../components/layout'
import SEO from '../components/seo'

const About = () => (
  <Layout>
  	<SEO title="Services" />
  	<div className="grid-container servicesPage">
	    <div>
		    <h2>Inspection services provided by HTI</h2>
		    <p>An Important part of your inspection is the report. Each inspection includes a <strong>detailed report</strong>, specifically tailored to that particular home that includes positive highlights as well as areas in need of repair, improvement, or maintenence.</p>
		    <p>A <strong>digital photo journal</strong> of the inspection is also included for your reference.</p>
		</div>
		<div>
			<h3>Areas included in inspection:</h3>
			<div className="serviceList">
				<div>
					<ul>
						<li>Grounds/site drainage</li>
						<li>Retaining walls, and fences</li>
						<li>Decks, porches, and railings</li>
						<li>Exterior plumbing and irrigation systems</li>
						<li>Gutters and downspouts</li>
						<li>Roof</li>
						<li>Interior plumbing and draining system</li>
						<li>Sump pump</li>
						<li>Water heater</li>
						<li>Central heating and cooling</li>
						<li>Electrical systems</li>
					</ul>
				</div>
				<div>
					<ul>
						<li>Service entrance</li>
						<li>Smoke alarms</li>
						<li>Fireplaces and chimney</li>
						<li>Floors, walls, and ceilings</li>
						<li>Doors and windows</li>
						<li>Kitchen(s) and bathroom(s)</li>
						<li>Attic ventilation and insulation</li>
						<li>Basement and crawlspace(s)</li>
						<li>Garage(s) and outbuildings</li>
						<li>Agricultural buildings (including horse facilities)</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
  </Layout>
)

export default About
