import React from 'react'
import { Link } from 'gatsby'

import Layout from '../components/layout'
import SEO from '../components/seo'

import detroit from '../images/detroit.jpg'

const About = () => (
  <Layout>
  	<SEO title="About" />
  	<div className="grid-container aboutPage">
	    <div>
	    	<img src={detroit} alt="Southeast Michigan certified inspector" />
	    </div>
	    <div>
		    <h2>The founder</h2>
		    <p><strong>Frank J. Buttermore</strong> is a Certified Professional Inspector® and certified by the <a target="_blank" href="https://www.nachi.org/certified-inspectors/frank-j-buttermore-6367">International Association of Certified Home Inspectors® (InterNACHI®)</a>.</p>
			<p>Frank's mission for HTI is simple: Providing an interactive, educational, and informative inspection. By including you in the inspection process, you can become more familiar with your prospective home.</p>
		</div>
		<div>
			<h2>The business</h2>
			<p>HTI Inspection Services LLC has been locally owned and owner-operated since 2005.</p><p>Serving Livingston County as well as ALL of Central and Southeast Lower Michigan. Detroit, Suburbs, and Livingston County business for over 30 years.</p>
			<p>Great membership standing with the International Association of Certified Home Inspectors and backed by InterNACHI's <a target="_blank" href="https://www.nachi.org/honor.htm">$10,000 Honor Guarantee</a>!</p>
		</div>
	</div>
  </Layout>
)

export default About
