import React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"
import Header from "../components/header"
import SEO from "../components/seo"

import home from "../images/house.jpg"
import office from "../images/office.jpg"
import inspector from "../images/home-inspection.jpg"

const IndexPage = () => (
  <Layout>
    <SEO title="Home" keywords={[`home inspection`, `commerical inspection`, `certified inspector`]} />
    <div className="homePage">
      <div>
        <h2>Residential inspections</h2>
        <p>Buying or selling a home is one of the most important decisions you will make in your lifetime. A professional home inspection is an integral part of your real estate transaction.</p>
        <p>Once you've found a home you're interested in, it's time to learn as much as you can about your potential investment.</p>
        <p>An inspection performed by an HTI certified home inspector will review all of the features, functions, systems, and structures inside and outside of the home.</p>
      </div>
      <div>
        <img src={home} alt="certified home inspection" />
      </div>
      <div>
        <h2>Commercial services</h2>
        <p>Maintaining workable standards for a business is crucial. Ensuring you have a professionally trained inspector could make all the difference when it comes to adhering to industry standards and regulations. </p>
      </div>
      <div>
        <img src={office} alt="certified commericial inspection" />
      </div>
      <div>
        <h3>What we do</h3>
        <ul>
          <li>Pre-Purchase Home Inspections (buyer)</li>
          <li>Pre-Listing Inspections (seller)</li>
          <li>Well and Septic Testing</li>
          <li>Investment Property Inspection</li>
          <li>Light Commericial/Retail Building Inspection</li>
          <li>Commercial Property Risk Assesment</li>
          <li><Link to="/services">...and more!</Link></li>
        </ul>
      </div>
      <div>
        <img src={inspector} alt="certified inspector services provided" />
      </div>
    </div>
  </Layout>
)

export default IndexPage
