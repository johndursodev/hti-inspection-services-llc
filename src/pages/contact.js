import React from 'react'
import { Link } from 'gatsby'
import { navigateTo } from "gatsby-link";

import Layout from '../components/layout'
import SEO from '../components/seo'


function encode(data) {
  return Object.keys(data)
    .map(key => encodeURIComponent(key) + "=" + encodeURIComponent(data[key]))
    .join("&");
}

export default class Contact extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  handleSubmit = e => {
    e.preventDefault();
    const form = e.target;
    fetch("/", {
      method: "POST",
      headers: { "Content-Type": "application/x-www-form-urlencoded" },
      body: encode({
        "form-name": form.getAttribute("name"),
        ...this.state
      })
    })
      .then(() => navigateTo(form.getAttribute("action")))
      .catch(error => alert(error));
  };

  render() {
    return (
        <Layout>
        <SEO title="Contact" keywords={[`home inspection`, `commerical inspection`, `certified inspector`]} />
          <div className="contactForm">
            <div className="aboveForm">
                <div>
                    <h2>Contact me</h2>
                    <p style={{fontWeight: `bold`,}}>We are available to answer your questions  anytime - 6am to 9pm. Schedule your Inspection today!</p>
                </div>
            </div>
            <form
              name="contact"
              method="post"
              action="/thanks/"
              data-netlify="true"
              data-netlify-honeypot="bot-field"
              onSubmit={this.handleSubmit}
            >
              {/* The `form-name` hidden field is required to support form submissions without JavaScript */}
              <input type="hidden" name="form-name" value="contact" />
              <p hidden>
                <label>
                  Don’t fill this out:{" "}
                  <input name="bot-field" onChange={this.handleChange} />
                </label>
              </p>
              <div className="formName">
                <label>
                  Your name:<br />
                  <input type="text" name="name" onChange={this.handleChange} />
                </label>
              </div>
              <div className="formEmail">
                <label>
                  Your email:<br />
                  <input type="email" name="email" onChange={this.handleChange} />
                </label>
              </div>
              <div className="formMessage">
                <label>
                  Message:<br />
                  <textarea name="message" onChange={this.handleChange} />
                </label>
              </div>
              <div className="formSubmit">
                <button type="submit">Send</button>
              </div>
            </form>
          </div>
        </Layout>
    );
  }
}