import { Link } from 'gatsby'
import React from 'react'
import './layout.scss'

import interNACHI from '../images/IACHI.png'
import commercialInspector from '../images/commercial-inspector.png';
import mobilehomeInspector from '../images/mobilehome-inspector.png';
import roofInspector from '../images/roof-inspector.png';
import electrical from '../images/electrical.png';
import safeInspector from '../images/safe-inspector.png';
import plumbing from '../images/plumbing.png';


const Footer = () => (
  <div>
  	<div className="contactUs">
		<h3>Contact us today for a free inspection quote!</h3>
	  	<p><Link to="mailto:homeinspector@ameritech.net"><span className="highlight">Email</span></Link> or <Link to="tel:5174042552"><span className="highlight">call</span></Link> us anytime for questions, a service quote, or to schedule your inspection! We look forward to hearing from you.</p>
	</div>
	<div className="footer">
        <h4>2019 &copy; HTI Inspection Services LLC, All rights reserved.</h4>
        <Link target="_blank" href="https://www.nachi.org/certified-inspectors/frank-j-buttermore-6367"><img src={interNACHI} alt="" />
        <img src={commercialInspector} alt="" />
        <img src={mobilehomeInspector} alt="" />
        <img src={roofInspector} alt="" />
        <img src={electrical} alt="" />
        <img src={safeInspector} alt="" />
        <img src={plumbing} alt="" /></Link>
	</div>
  </div>
)

export default Footer