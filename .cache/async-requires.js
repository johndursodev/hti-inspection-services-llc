// prefer default export if available
const preferDefault = m => m && m.default || m

exports.components = {
  "component---src-pages-404-js": () => import("/Users/john/Desktop/gatsby/hti-inspection-services-llc/src/pages/404.js" /* webpackChunkName: "component---src-pages-404-js" */),
  "component---src-pages-about-js": () => import("/Users/john/Desktop/gatsby/hti-inspection-services-llc/src/pages/about.js" /* webpackChunkName: "component---src-pages-about-js" */),
  "component---src-pages-contact-js": () => import("/Users/john/Desktop/gatsby/hti-inspection-services-llc/src/pages/contact.js" /* webpackChunkName: "component---src-pages-contact-js" */),
  "component---src-pages-index-js": () => import("/Users/john/Desktop/gatsby/hti-inspection-services-llc/src/pages/index.js" /* webpackChunkName: "component---src-pages-index-js" */),
  "component---src-pages-page-2-js": () => import("/Users/john/Desktop/gatsby/hti-inspection-services-llc/src/pages/page-2.js" /* webpackChunkName: "component---src-pages-page-2-js" */),
  "component---src-pages-services-js": () => import("/Users/john/Desktop/gatsby/hti-inspection-services-llc/src/pages/services.js" /* webpackChunkName: "component---src-pages-services-js" */)
}

exports.data = () => import(/* webpackChunkName: "pages-manifest" */ "/Users/john/Desktop/gatsby/hti-inspection-services-llc/.cache/data.json")

